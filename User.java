public class User {
    private String username;
    private String password;
    private UserType type;

    public User(String username, String password, UserType type){
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public UserType getType(){
        return this.type;
    }

}